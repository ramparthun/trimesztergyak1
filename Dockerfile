FROM ubuntu:latest
RUN apt update
RUN apt install -y build-essential cmake
COPY . /source
WORKDIR /source/build
RUN cmake ..
RUN make
ENTRYPOINT [ "/source/build/trim1" ]
