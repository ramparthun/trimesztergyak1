#include "controller.h"
#include "json.hpp"
#include <iostream>

std::map<int,QString> Controller::data;
std::mutex Controller::m;
int Controller::nextId=0;

Controller::Controller()
{

}

void Controller::get(const Request &request, Response &response)
{
    m.lock();
    auto  reqid=request.matches[1].str();
    int id=0;
    QString adat;
    try{
       id=std::stoi(reqid);
       adat=data.at(id);
       nlohmann::json resBody={{"id",id},{"data",adat.toStdString()}};
       response.set_content(resBody.dump(),"application/json");
    }catch(...){    //catch EVERYTHING but we dont care what it is
        if(data.size()<=id){
            response.status=404;
        }else {
            response.status=400; //bad request
        }
    }
    m.unlock();
}
void Controller::getAll(const Request &request, Response &response)
{
    m.lock(); //mutex needs init
    nlohmann::json resBody=nlohmann::json::array({});//json needs this namespace
    for(const auto &it:data){
        resBody.push_back({{"id",it.first},{"data",it.second.toStdString()}}); //QSTRING conversion!!!
    }
    response.set_content(resBody.dump(),"application/json");//return format and function form
    m.unlock();
}
void Controller::add(const Request &request, Response &response)
{
    m.lock();
    try {
        std::cout<<request.body<<std::endl;
        nlohmann::json reqBody=nlohmann::json::parse(request.body);//convert string to json
        QString assist=QString::fromStdString(reqBody.at("data"));
        data.emplace(nextId,assist);
        nlohmann::json resBody={{"id",nextId},{"data",reqBody.at("data")}};
        nextId++;
        response.set_content(resBody.dump(),"application/json");
    } catch (...) {
        response.status=400;
    }
    m.unlock();
}
void Controller::remove(const Request &request, Response &response)
{
    m.lock();
    auto  reqid=request.matches[1].str();
    int id=0;
    QString adat;
    try{
       id=std::stoi(reqid);
       adat=data.at(id);
       nlohmann::json resBody={{"id",id},{"data",adat.toStdString()}};
       data.erase(id);
       response.set_content(resBody.dump(),"application/json");
    }catch(...){
        if(data.find(id)==data.end()){
            response.status=404;
        }else {
            response.status=200; //OK
        }
    }
    m.unlock();
}
void Controller::modify(const Request &request, Response &response)
{
    m.lock();
    auto  reqid=request.matches[1].str();
    int id=0;
    QString adat;
    try {
        id=std::stoi(reqid);
        nlohmann::json reqBody=nlohmann::json::parse(request.body);
        QString assist=QString::fromStdString(reqBody.at("data"));
        data.emplace(id,assist);
        nlohmann::json resBody={{"id",id},{"data",reqBody.at("data")}};
        response.set_content(resBody.dump(),"application/json");
    } catch (...) {
        if(data.find(id)==data.end()){
            response.status=404;
        }else {
            response.status=200;
        }
    }
    m.unlock();
}
