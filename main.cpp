#include <iostream>
#include "controller.h"
int main (){
    using namespace httplib;
    Server server;  //Server httplib type
    server.Get(R"(/data)",Controller::getAll);
    server.Get(R"(/data/(\d+))",Controller::get);
    server.Put(R"(/data/(\d+))",Controller::modify);
    server.Delete(R"(/data/(\d+))",Controller::remove);
    server.Post(R"(/data/(\d+))",Controller::add);
    //LOG

    std::cout<<"server listen on: http://0.0.0.0:3000"<<std::endl;
    server.listen("0.0.0.0",3000);
    return 0;
}
