#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "httplib.h"
#include <QString>
#include <map>
#include <mutex>
using namespace httplib;
class Controller
{
    static std::map<int,QString> data;
    static std::mutex m;
    static int nextId;
public:
    Controller(); //empty
    static void get(const Request &request,Response &response);
    static void getAll(const Request &request,Response &response);
    static void add(const Request &request,Response &response);
    static void remove(const Request &request,Response &response);
    static void modify(const Request &request,Response &response);
};

#endif // CONTROLLER_H
